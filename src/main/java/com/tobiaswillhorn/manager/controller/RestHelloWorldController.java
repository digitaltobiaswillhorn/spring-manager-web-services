package com.tobiaswillhorn.manager.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
public class RestHelloWorldController {
    @GetMapping("/world")
    public String index(){
        return "Hello world";
    }
}
